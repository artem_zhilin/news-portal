/* global jest */

const axios = jest.genMockFromModule('axios');
const newsToFetch = global.news.slice(0, 5);
const axiosGet = (url) => {
  const resolveValue = { data: newsToFetch };
  if (url.indexOf('NBUStatService') !== -1) {
    resolveValue.data = [{ rate: global.rate }];
  }
  return Promise.resolve(resolveValue);
};
axios.all.mockImplementation(requests => Promise.all(requests));
axios.get.mockImplementation(axiosGet);
axios.mockImplementation(axiosGet);

global.axios = axios;

export default axios;

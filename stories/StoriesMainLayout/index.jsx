import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import styles from '../../src/layouts/MainLayout/styles';
import SelectLanguage from '../../src/components/SelectLanguage';

const MainLayout = ({ classes, children }) => (
  <Router>
    <React.Fragment>
      <CssBaseline />
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <SelectLanguage />
      <main className={classes.layout}>
        {children}
      </main>
    </React.Fragment>
  </Router>
);

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default withStyles(styles)(MainLayout);

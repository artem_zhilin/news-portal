/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { storiesOf } from '@storybook/react';

import { Grid } from '@material-ui/core';
import RatesWidget from '../src/components/RatesWidget';

storiesOf('RatesWidget', module)
  .add('with USD and Euro', ({ t }) => {
    const rates = {
      usd: '34.34',
      euro: '45.91',
    };
    return (
      <Grid container>
        <Grid item md={4}>
          <RatesWidget t={t} rates={rates} />
        </Grid>
      </Grid>
    );
  });

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { storiesOf } from '@storybook/react';

import SignInModal from '../src/components/SignInModal';

storiesOf('SignInModal', module)
  .add('opened modal', ({ t }) => (
    <SignInModal opened t={t} handleClose={() => {}} />
  ));

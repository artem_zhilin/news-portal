/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { storiesOf } from '@storybook/react';

import { Grid } from '@material-ui/core';
import moment from 'moment';
import LatestNews from '../src/components/LatestNews';
import EditorChoiceNews from '../src/components/EditorChoiceNews';
import PopularNews from '../src/components/PopularNews';
import LatestMediaNews from '../src/components/LatestMediaNews';

storiesOf('News', module)
  .addDecorator((storyFn) => {
    const news = [
      {
        _id: '1',
        title: 'Заголовок первой новости без медиа файлов еще чуть текста',
        viewsCount: 12,
        commentsCount: 3,
        createdAt: moment('2019-05-19T16:34:34Z'),
        tags: [{ name: 'Экономика' }],
        hasPhotos: false,
        hasVideo: false,
        photos: [],
        mainImage: { url: '/uploads/496cf5a2975d4d1ca3ec1ebb84a53b15.jpg' },
        video: null,
      },
      {
        _id: '2',
        title: 'Заголовок второй новости без видео но с фото еще чуть текста',
        viewsCount: 2,
        commentsCount: 0,
        createdAt: moment('2019-05-19T12:20:34Z'),
        tags: [{ name: 'Политика' }],
        hasPhotos: true,
        hasVideo: false,
        photos: [1],
        mainImage: { url: '/uploads/811fd611953d4d4684031f04e1d32a45.jpg' },
        video: null,
      },
      {
        _id: '3',
        title: 'Заголовок третей новости без фото но с видео еще чуть текста и еще чуть текста и еще',
        viewsCount: 0,
        commentsCount: 0,
        createdAt: moment('2019-05-18T10:15:34Z'),
        tags: [{ name: 'Авто' }],
        mainImage: { url: '/uploads/c7cc3c3f7a044729947c5c772a58cde1.jpg' },
        photos: [],
        video: 'asd',
        hasPhotos: false,
        hasVideo: true,
      },
      {
        _id: '4',
        title: 'Заголовок четвертой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
        viewsCount: 20,
        commentsCount: 5,
        mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
        createdAt: moment('2019-05-18T07:05:34Z'),
        tags: [{ name: 'Украина' }],
        photos: [1],
        video: 'asd',
        hasPhotos: true,
        hasVideo: true,
      },
      {
        _id: '5',
        title: 'Заголовок пятой новости в котором будет много текста много текста много текста много текста еще много текста еще больше текста',
        viewsCount: 10,
        commentsCount: 2,
        createdAt: moment('2019-05-18T06:49:34Z'),
        tags: [{ name: 'Бизнес' }],
        mainImage: { url: '/uploads/3a236d156c53463ba3e9f48e1b1df387.jpg' },
        photos: [],
        video: null,
        hasPhotos: false,
        hasVideo: false,
      },
      {
        _id: '6',
        title: 'Заголовок шестой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
        viewsCount: 88,
        commentsCount: 28,
        mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
        createdAt: moment('2019-05-20T17:05:34Z'),
        tags: [{ name: 'Украина' }],
        photos: [1],
        video: 'asd',
        hasPhotos: true,
        hasVideo: true,
      },
      {
        _id: '7',
        title: 'Заголовок седьмой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
        viewsCount: 91,
        commentsCount: 21,
        mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
        createdAt: moment('2019-05-20T17:05:34Z'),
        tags: [{ name: 'Украина' }],
        photos: [1],
        video: 'asd',
        hasPhotos: true,
        hasVideo: true,
      },
      {
        _id: '8',
        title: 'Заголовок восьмой новости без фото но с видео еще чуть текста и еще чуть текста и еще',
        viewsCount: 0,
        commentsCount: 0,
        createdAt: moment('2019-05-15T19:20:34Z'),
        tags: [{ name: 'Авто' }],
        mainImage: { url: '/uploads/c7cc3c3f7a044729947c5c772a58cde1.jpg' },
        photos: [],
        video: 'asd',
        hasPhotos: false,
        hasVideo: true,
      },
      {
        _id: '9',
        title: 'Заголовок девятой новости без видео но с фото еще чуть текста',
        viewsCount: 2,
        commentsCount: 0,
        createdAt: moment('2019-05-19T10:20:34Z'),
        tags: [{ name: 'Политика' }],
        photos: [1],
        mainImage: { url: '/uploads/811fd611953d4d4684031f04e1d32a45.jpg' },
        video: null,
        hasPhotos: true,
        hasVideo: false,
      },
    ];
    return storyFn({ news });
  })
  .add('latest news', ({ t, news }) => (
    <Grid container>
      <Grid item lg={4} sm={6} xs={6} md={4}>
        <LatestNews t={t} latestNews={news.slice(0, 5)} />
      </Grid>
    </Grid>
  ))
  .add('editor choice news', ({ t, news }) => (
    <Grid container>
      <Grid item lg={8} sm={12} xs={12} md={6}>
        <EditorChoiceNews t={t} editorChoiceNews={news.slice(0, 6)} />
      </Grid>
    </Grid>
  ))
  .add('popular news', ({ t, news }) => (
    <Grid container>
      <Grid item lg={8} sm={12} xs={12} md={6}>
        <PopularNews t={t} popularNews={{ views: news.slice(0, 5), comments: news.slice(5, 10) }} />
      </Grid>
    </Grid>
  ))
  .add('latest media news', ({ t, news }) => (
    <Grid container>
      <Grid item lg={4} sm={6} xs={6} md={4}>
        <LatestMediaNews t={t} latestMediaNews={{ photoNews: news[1], videoNews: news[2] }} />
      </Grid>
    </Grid>
  ));

/* eslint-disable import/prefer-default-export */
export const getCurrencyRates = ({ common }) => common.currencyRates;

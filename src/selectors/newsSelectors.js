export const getNews = ({ news }) => news.news;
export const getLatestNews = ({ news }) => news.latestNews;
export const getEditorChoiceNews = ({ news }) => news.editorChoiceNews;
export const getPopularNews = ({ news }) => news.popularNews;
export const getLatestMediaNews = ({ news }) => news.latestMediaNews;

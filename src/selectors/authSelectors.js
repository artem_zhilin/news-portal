export const getToken = ({ auth }) => auth.token;

export const getIsAuthenticated = ({ auth }) => auth.isAuthenticated;

export const getAuthenticatedUser = ({ auth }) => auth.user;

export const getErrors = ({ auth }) => auth.errors;

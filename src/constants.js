import PropTypes from 'prop-types';
import facebook from './assets/img/facebook.png';
import google from './assets/img/google.png';
import telegram from './assets/img/telegram.png';
import instagram from './assets/img/instagram.png';
import twitter from './assets/img/twitter.png';
import youtube from './assets/img/youtube.png';

export const TAG_SHAPE = PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
});

export const NEWS_SHAPE = PropTypes.shape({
  _id: PropTypes.string,
  mainImage: PropTypes.object,
  title: PropTypes.string,
  body: PropTypes.string,
  viewsCount: PropTypes.number,
  commentsCount: PropTypes.number,
  tags: PropTypes.arrayOf(TAG_SHAPE),
  hasPhotos: PropTypes.bool,
  hasVideo: PropTypes.bool,
  video: PropTypes.object,
  photos: PropTypes.arrayOf(PropTypes.object),
  slug: PropTypes.slug,
});

export const CURRENCY_RATES_SHAPE = PropTypes.shape({
  usd: PropTypes.string,
  euro: PropTypes.string,
});

export const LATEST_MEDIA_NEWS_SHAPE = PropTypes.shape({
  photoNews: NEWS_SHAPE,
  videoNews: NEWS_SHAPE,
});

export const socialLinks = [
  {
    provider: 'facebook',
    icon: facebook,
    link: '',
  },
  {
    provider: 'google',
    icon: google,
    link: '',
  },
  {
    provider: 'instagram',
    icon: instagram,
    link: '',
  },
  {
    provider: 'telegram',
    icon: telegram,
    link: '',
  },
  {
    provider: 'twitter',
    icon: twitter,
    link: '',
  },
  {
    provider: 'youtube',
    icon: youtube,
    link: '',
  },
];

/* eslint-disable func-names */
/* eslint-disable no-undef */
import moment from 'moment';

export const transformInParams = (field, values) => `?${values.map(value => `${field}_in=${value}`).join('&')}`;

export const transformDates = {
  responseType: 'text',
  transformResponse: res => (
    JSON.parse(res, (key, value) => (key === 'createdAt' || key === 'updatedAt' ? moment(value) : value))
  ),
};

export const transformNews = {
  responseType: 'text',
  transformResponse: res => (
    JSON.parse(res, (key, value) => {
      if (key === 'createdAt' || key === 'updatedAt') {
        return moment(value);
      }
      if (key === 'body') {
        return value.replace(/(?:\r\n|\r|\n)/g, '<br>');
      }
      return value;
    })
  ),
};

export const insertScript = (src, id, parentElement) => {
  const script = window.document.createElement('script');
  script.async = true;
  script.src = src;
  script.id = id;
  parentElement.appendChild(script);

  return script;
};

export const removeScript = (id, parentElement) => {
  const script = window.document.getElementById(id);
  if (script) { parentElement.removeChild(script); }
};

export const debounce = (func, wait, immediate) => {
  let timeout;

  return function executedFunction() {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);

    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};

import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import store from './store';
import {
//  PrivateRoutes,
  PublicRoutes,
} from './routes';
import PublicRoute from './routes/PublicRoute';
import NotFound from './pages/NotFound';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        {PublicRoutes.map(route => (
          <PublicRoute
            key={route.path}
            layout={route.layout}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
        {/* {PrivateRoutes.map(route => (
          <PrivateRoute
            key={route.path}
            layout={route.layout}
            forOwner={route.forOwner}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))} */}
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  </Provider>
);

export default App;

import MainLayout from '../layouts/MainLayout';
import Dashboard from '../pages/Dashboard';
import News from '../pages/News';

export const DASHBOARD = '/';
export const SIGN_IN = '/#login';
export const NEWS_VIEW = '/news/:id/:slug';

export const NavBarLinks = [
  {
    title: 'Main page',
    path: DASHBOARD,
  },
  {
    title: 'Photo report',
    path: DASHBOARD,
  },
  {
    title: 'Video report',
    path: DASHBOARD,
  },
  {
    title: 'Politics',
    path: DASHBOARD,
  },
  {
    title: 'Crime',
    path: DASHBOARD,
  },
  {
    title: 'Sport',
    path: DASHBOARD,
  },
  {
    title: 'Culture',
    path: DASHBOARD,
  },
  {
    title: 'Archive',
    path: DASHBOARD,
  },
  {
    title: 'Contacts',
    path: DASHBOARD,
  },
];

export const PublicRoutes = [
  {
    title: 'Dashboard',
    path: DASHBOARD,
    layout: MainLayout,
    exact: true,
    component: Dashboard,
  },
  {
    title: 'News',
    path: NEWS_VIEW,
    layout: MainLayout,
    exact: true,
    component: News,
  },
];

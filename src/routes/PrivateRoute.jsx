import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { DASHBOARD } from '.';
import { getIsAuthenticated } from '../selectors/authSelectors';

const PrivateRoute = ({
  isAuthenticated, layout: Layout, component: Component, ...rest
}) => {
  const getRenderValue = (props) => {
    let renderValue = (
      <Redirect to={{
        pathname: DASHBOARD,
        state: { from: props.location },
      }}
      />
    );
    if (isAuthenticated) {
      renderValue = <Layout><Component {...props} /></Layout>;
    }
    return renderValue;
  };
  return <Route {...rest} render={getRenderValue} />;
};

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(React.Component),
  ]).isRequired,
  layout: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(React.Component),
  ]).isRequired,
};

const mapStateToProps = state => ({
  isAuthenticated: getIsAuthenticated(state),
});

export default connect(mapStateToProps, {})(PrivateRoute);

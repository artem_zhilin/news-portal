/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import rootReducer from './reducers';

const initialState = {};

const middleware = [thunk];

const composeEnhancers = typeof window === 'object'
  && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
  }) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
);

const store = createStore(rootReducer, initialState, enhancer);

store.subscribe(() => {
  const { auth } = store.getState();
  if (!localStorage.getItem('auth') && auth.isAuthenticated) {
    axios.defaults.headers.common.Authorization = `Bearer ${auth.token}`;
    localStorage.setItem('auth', JSON.stringify(auth));
  } else if (localStorage.getItem('auth') && !auth.isAuthenticated) {
    localStorage.removeItem('auth');
  }
});

export default store;

export const DO_AUTH = 'DO_AUTH';
export const DO_LOGOUT = 'DO_LOGOUT';
export const UPDATE_USER = 'UPDATE_USER';
export const REQUEST_ERROR_AUTH = 'REQUEST_ERROR_AUTH';
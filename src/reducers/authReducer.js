/* eslint-disable no-undef */
import {
  DO_AUTH, DO_LOGOUT, UPDATE_USER, REQUEST_ERROR_AUTH,
} from '../types/authTypes';

const initialState = {
  token: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).token : '',
  isAuthenticated: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).isAuthenticated : false,
  user: JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).user : {},
  errors: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case DO_AUTH: {
      return {
        ...state,
        token: action.payload.token,
        isOwner: action.payload.isOwner,
        user: action.payload.user,
        isAuthenticated: true,
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }
    case REQUEST_ERROR_AUTH: {
      return {
        ...state,
        errors: action.payload,
      };
    }
    case DO_LOGOUT: {
      return {
        token: '', user: {}, isAuthenticated: false, errors: {},
      };
    }
    default:
      return state;
  }
}

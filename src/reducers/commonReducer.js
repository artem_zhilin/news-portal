import {
  FETCH_CURRENCY_RATES,
} from '../types/commonTypes';

const initialState = {
  currencyRates: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_CURRENCY_RATES: {
      return {
        ...state,
        currencyRates: action.payload,
      };
    }
    default:
      return state;
  }
}

import { combineReducers } from 'redux';
import authReducer from './authReducer';
import newsReducer from './newsReducer';
import commonReducer from './commonReducer';

export default combineReducers({
  auth: authReducer,
  news: newsReducer,
  common: commonReducer,
});

import {
  FETCH_LATEST_NEWS,
  FETCH_NEWS,
  FETCH_EDITOR_CHOICE_NEWS,
  FETCH_POPULAR_NEWS,
  FETCH_LATEST_MEDIA_NEWS,
  NEWS_INCREASE_COMMENTS,
} from '../types/newsTypes';

const initialState = {
  latestNews: [],
  editorChoiceNews: [],
  popularNews: { views: [], comments: [] },
  news: { tags: [], photos: [], mainImage: {} },
  latestMediaNews: { photoNews: {}, videoNews: {} },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_POPULAR_NEWS:
      return {
        ...state,
        popularNews: action.payload,
      };
    case FETCH_LATEST_NEWS: {
      return {
        ...state,
        latestNews: action.payload,
      };
    }
    case FETCH_EDITOR_CHOICE_NEWS: {
      return {
        ...state,
        editorChoiceNews: action.payload,
      };
    }
    case FETCH_LATEST_MEDIA_NEWS: {
      return {
        ...state,
        latestMediaNews: action.payload,
      };
    }
    case FETCH_NEWS: {
      return {
        ...state,
        news: action.payload,
      };
    }
    case NEWS_INCREASE_COMMENTS: {
      return {
        ...state,
        news: { ...state.news, commentsCount: ++state.news.commentsCount },
      };
    }
    default:
      return state;
  }
}

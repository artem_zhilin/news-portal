const styles = theme => ({
  navBar: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  logo: {
    padding: '10px',
  },
  btnSignOut: {
    marginLeft: '20px',
    [theme.breakpoints.down('xs')]: {
      marginLeft: '5px',
    },
  },
  socialIcons: {
    marginRight: 5,
    borderRadius: '50%',
    width: '32px',
    cursor: 'pointer',
    '&:last-child': {
      marginRight: 0,
    },
  },
  actionIcons: {
    marginRight: 30,
    color: theme.palette.primary.main,
    fontSize: 32,
    verticalAlign: 'initial !important',
    cursor: 'pointer',
  },
  selectLanguage: {
    marginRight: 40,
    color: theme.palette.common.black,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    padding: '0 !important',
  },
  toolbarContainer: {
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1200)]: {
      width: 1140,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
});

export default styles;

import React, { useState } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import * as PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import {
  Typography,
  Grid,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import {
  DASHBOARD,
  NavBarLinks,
} from '../../routes';
import { socialLinks } from '../../constants';
import SelectLanguage from '../SelectLanguage';
import styles from './styles';
import logo from '../../assets/img/logo.svg';
import SignInModal from '../SignInModal';

const NavBar = ({
  classes,
}) => {
  const { t } = useTranslation();
  const [signInModalOpened, setSignInModalOpened] = useState(false);

  const handleSignInModalOpen = () => setSignInModalOpened(true);
  const handleSignInModalClose = () => setSignInModalOpened(false);


  return (
    <AppBar className={classes.appBar}>
      <Toolbar>
        <Grid container alignItems="center" justify="space-between" direction="row">
          <Grid item>
            <Link to={DASHBOARD}>
              <img className={classes.logo} src={logo} alt="logo" />
            </Link>
          </Grid>
          <Grid item>
            <i className={`${classes.actionIcons} material-icons`}>search</i>
            <i tabIndex={0} role="button" className={`${classes.actionIcons} material-icons`} onClick={handleSignInModalOpen}>
              account_circle
            </i>
            <SelectLanguage />
            {socialLinks.map(socialLink => (
              <img key={socialLink.provider} className={classes.socialIcons} src={socialLink.icon} alt={`${socialLink.provider}Logo`} />
            ))}
          </Grid>
        </Grid>
      </Toolbar>
      <Toolbar className={classes.navBar}>
        <Grid container className={classes.toolbarContainer} alignItems="center" alignContent="center" justify="space-evenly" direction="row">
          {NavBarLinks.map(link => (
            <Link key={link.title} to={link.path}>
              <Typography variant="subtitle1" color="inherit">
                {t(link.title)}
              </Typography>
            </Link>
          ))}
        </Grid>
      </Toolbar>
      <SignInModal t={t} opened={signInModalOpened} handleClose={handleSignInModalClose} />
    </AppBar>
  );
};

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);

/* global describe, it, expect, customRender, axios, rate */
import React from 'react';
import { waitForDomChange } from 'react-testing-library';
import SideBar from './index';

describe('SideBar', () => {
  it('should fetch data correctly', () => {
    customRender(<SideBar />);
    expect(axios.all).toHaveBeenCalledTimes(2);
    expect(axios).toHaveBeenCalledTimes(5);
  });

  it('should render fetched data correctly', async () => {
    const { getByTestId, container, getByAltText } = customRender(<SideBar />);
    await waitForDomChange(() => getByTestId('news-container'));
    const usdContainer = getByAltText('usd');
    const newsContainer = getByTestId('news-container');
    expect(usdContainer.nextSibling).toHaveTextContent(rate);
    expect(newsContainer.children).toHaveLength(5);
    expect(container).toMatchSnapshot();
  });
});

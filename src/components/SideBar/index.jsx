import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Grid,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import LatestNews from '../LatestNews';
import WeatherWidget from '../WeatherWidget';
import RatesWidget from '../RatesWidget';
import LatestMediaNews from '../LatestMediaNews';
import { CURRENCY_RATES_SHAPE, LATEST_MEDIA_NEWS_SHAPE } from '../../constants';
import { getLatestNews, getLatestMediaNews } from '../../selectors/newsSelectors';
import { getCurrencyRates } from '../../selectors/commonSelectors';
import {
  fetchLatestNews,
  fetchLatestMediaNews,
} from '../../actions/newsActions';
import { fetchCurrencyRates } from '../../actions/commonActions';


const SideBar = ({
  latestNews,
  currencyRates,
  latestMediaNews,
  fetchLatestMediaNewsDispatch,
  fetchCurrencyRatesDispatch,
  fetchLatestNewsDispatch,
}) => {
  const { t, i18n } = useTranslation();

  useEffect(() => {
    fetchLatestMediaNewsDispatch();
    fetchLatestNewsDispatch();
    fetchCurrencyRatesDispatch();
    // eslint-disable-next-line
  }, []);

  return (
    <Grid container direction="column">
      <Grid className="m-b" container direction="column">
        <LatestNews t={t} latestNews={latestNews} />
      </Grid>
      <Grid className="m-b" item>
        <WeatherWidget t={t} i18n={i18n} />
      </Grid>
      <Grid className="m-b" item>
        <RatesWidget t={t} rates={currencyRates} />
      </Grid>
      <Grid className="m-b" item>
        <LatestMediaNews t={t} latestMediaNews={latestMediaNews} />
      </Grid>
    </Grid>
  );
};

SideBar.propTypes = {
  latestNews: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchCurrencyRatesDispatch: PropTypes.func.isRequired,
  fetchLatestMediaNewsDispatch: PropTypes.func.isRequired,
  fetchLatestNewsDispatch: PropTypes.func.isRequired,
  currencyRates: CURRENCY_RATES_SHAPE.isRequired,
  latestMediaNews: LATEST_MEDIA_NEWS_SHAPE.isRequired,
};

const mapDispatchToProps = {
  fetchCurrencyRatesDispatch: fetchCurrencyRates,
  fetchLatestNewsDispatch: fetchLatestNews,
  fetchLatestMediaNewsDispatch: fetchLatestMediaNews,
};

const mapStateToProps = state => ({
  latestNews: getLatestNews(state),
  latestMediaNews: getLatestMediaNews(state),
  currencyRates: getCurrencyRates(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);

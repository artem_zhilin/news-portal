const styles = theme => ({
  newsItem: {
    marginBottom: '10px',
    '&:nth-child(3n+2)': {
      padding: '0 10px',
    },
    '&:nth-child(3n+3)': {
      paddingLeft: '10px',
    },
    '&:nth-child(3n+1)': {
      paddingRight: '10px',
    },
    [theme.breakpoints.down('md')]: {
      '&:nth-child(even)': {
        paddingLeft: '10px',
        paddingRight: 0,
      },
      '&:nth-child(odd)': {
        paddingRight: '10px',
        paddingLeft: 0,
      },
    },
  },
  newsDate: {
    fontSize: '0.8em',
    color: '#757575',
  },
  newsStats: {
    fontSize: '0.8em',
    color: '#757575',
  },
  newsTitle: {
    fontWeight: 600,
    fontSize: '1em',
  },
  newsImage: {
    width: '100%',
    height: '150px',
  },
});

export default styles;

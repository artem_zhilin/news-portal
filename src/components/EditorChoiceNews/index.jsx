import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './styles';
import SectionTitle from '../SectionTitle';
import { NEWS_VIEW } from '../../routes';
import NewsStats from '../NewsStats';
import { NEWS_SHAPE } from '../../constants';
import CommentsCount from '../Disqus/CommentsCount';

const EditorChoiceNews = ({ classes, t, editorChoiceNews }) => (
  <React.Fragment>
    <SectionTitle>{t('Editor choice')}</SectionTitle>
    <Grid container>
      {editorChoiceNews.map(news => (
        <Grid key={news._id} className={classes.newsItem} item lg={4} md={6} sm={6} xs={6}>
          <Link to={NEWS_VIEW.replace(':id', news._id).replace(':slug', news.slug)}>
            <img
              className={classes.newsImage}
              src={process.env.REACT_APP_MEDIA_BASE_URL + (news.mainImage ? news.mainImage.url : process.env.REACT_APP_MEDIA_BASE_URL)}
              alt={news.title}
            />
            <Grid justify="space-between" container>
              <NewsStats
                viewsCount={news.viewsCount}
                commentsCount={(
                  <CommentsCount
                    identifier={news._id}
                    title={news.title}
                  />
                )}
                createdAt={news.createdAt}
                classes={{ stats: classes.newsStats, date: classes.newsDate }}
                t={t}
              />
            </Grid>
            <Typography className={classes.newsTitle}>
              {news.title}
            </Typography>
          </Link>
        </Grid>
      ))}
    </Grid>
  </React.Fragment>
);

EditorChoiceNews.propTypes = {
  classes: PropTypes.object.isRequired,
  editorChoiceNews: PropTypes.arrayOf(NEWS_SHAPE).isRequired,
  t: PropTypes.func.isRequired,
};


export default withStyles(styles)(EditorChoiceNews);

import React from 'react';
import PropTypes from 'prop-types';
import { Audio } from 'react-loader-spinner/dist/loader/Audio';
import { BallTriangle } from 'react-loader-spinner/dist/loader/BallTriangle';
import { Bars } from 'react-loader-spinner/dist/loader/Bars';
import { Circles } from 'react-loader-spinner/dist/loader/Circles';
import { Grid } from 'react-loader-spinner/dist/loader/Grid';
import { Hearts } from 'react-loader-spinner/dist/loader/Hearts';
import { Oval } from 'react-loader-spinner/dist/loader/Oval';
import { Puff } from 'react-loader-spinner/dist/loader/Puff';
import { Rings } from 'react-loader-spinner/dist/loader/Rings';
import { TailSpin } from 'react-loader-spinner/dist/loader/TailSpin';
import { ThreeDots } from 'react-loader-spinner/dist/loader/ThreeDots';
import { Watch } from 'react-loader-spinner/dist/loader/Watch';
import { RevolvingDot } from 'react-loader-spinner/dist/loader/RevolvingDot';
import { CradleLoader } from 'react-loader-spinner/dist/loader/CradleLoader';
import { Triangle } from 'react-loader-spinner/dist/loader/Triangle';
import { Plane } from 'react-loader-spinner/dist/loader/Plane';
import { MutatingDot } from 'react-loader-spinner/dist/loader/MutatingDot';

const Loader = (props) => {
  const { className, type } = props;

  const svgRenderer = (loaderType) => {
    switch (loaderType) {
      case 'Audio':
        return <Audio {...props} />;
      case 'Ball-Triangle':
        return <BallTriangle {...props} />;
      case 'Bars':
        return <Bars {...props} />;
      case 'Circles':
        return <Circles {...props} />;
      case 'Grid':
        return <Grid {...props} />;
      case 'Hearts':
        return <Hearts {...props} />;
      case 'Oval':
        return <Oval {...props} />;
      case 'Puff':
        return <Puff {...props} />;
      case 'Rings':
        return <Rings {...props} />;
      case 'TailSpin':
        return <TailSpin {...props} />;
      case 'ThreeDots':
        return <ThreeDots {...props} />;
      case 'Watch':
        return <Watch {...props} />;
      case 'RevolvingDot':
        return <RevolvingDot {...props} />;
      case 'CradleLoader':
        return <CradleLoader {...props} />;
      case 'Triangle':
        return <Triangle {...props} />;
      case 'Plane':
        return <Plane {...props} />;
      case 'MutatingDot':
        return <MutatingDot {...props} />;
      default:
        return (
          <div>
            <span style={{ color: 'Green' }}>LOADING</span>
            <small>
              <i>
                <br />
                Note:No specific svg type exist
              </i>
            </small>
          </div>
        );
    }
  };

  return (
    <div className={className}>{svgRenderer(type)}</div>
  );
};

Loader.propTypes = {
  color: PropTypes.string,
  type: PropTypes.string,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  className: PropTypes.string,
};

Loader.defaultProps = {
  color: '#3f51b5',
  type: 'Puff',
  height: 100,
  width: 100,
  className: 'loader-spinner',
};

export default Loader;

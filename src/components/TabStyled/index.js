import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';

const TabStyled = withStyles(theme => ({
  root: {
    textTransform: 'none',
    fontSize: '1em',
  },
  selected: {
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    borderTopLeftRadius: '5px',
    borderTopRightRadius: '5px',
  },
}))(Tab);

export default TabStyled;

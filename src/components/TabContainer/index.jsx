import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const TabContainer = withStyles(theme => ({
  root: {
    borderTop: `2px solid ${theme.palette.primary.main}`,
    borderBottom: `2px solid ${theme.palette.primary.main}`,
    boxShadow: 'none',
    borderRadius: 0,
  },
}))(Paper);

export default TabContainer;

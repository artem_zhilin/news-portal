import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const SectionTitle = withStyles({
  root: {
    fontWeight: 600,
    borderLeft: '5px solid #89bf4d',
    paddingLeft: '20px',
    fontSize: '1.25em',
    letterSpacing: '0.0075em',
    marginBottom: '10px',
  },
})(Typography);

export default SectionTitle;

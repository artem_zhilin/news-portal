import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Tabs } from '@material-ui/core';
import { LATEST_MEDIA_NEWS_SHAPE } from '../../constants';
import TabStyled from '../TabStyled';
import LatestMediaNewsTab from './LatestMediaNewsTab';

const LatestMediaNews = ({ latestMediaNews, t }) => {
  const [tabValue, setTabValue] = useState('photo');

  const handleTabChange = (e, value) => {
    setTabValue(value);
  };

  return (
    <React.Fragment>
      <Tabs indicatorColor="primary" value={tabValue} onChange={handleTabChange}>
        <TabStyled data-testid="photo-tab" value="photo" label={t('Photo')} />
        <TabStyled data-testid="video-tab" value="video" label={t('Video')} />
      </Tabs>
      {tabValue === 'photo' && (
        <LatestMediaNewsTab t={t} news={latestMediaNews.photoNews} />
      )}
      {tabValue === 'video' && (
        <LatestMediaNewsTab t={t} news={latestMediaNews.videoNews} />
      )}
    </React.Fragment>
  );
};

LatestMediaNews.propTypes = {
  t: PropTypes.func.isRequired,
  latestMediaNews: LATEST_MEDIA_NEWS_SHAPE.isRequired,
};

export default LatestMediaNews;

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styles from './styles';
import TabContainer from '../TabContainer';
import { NEWS_VIEW } from '../../routes';
import NewsStats from '../NewsStats';
import { NEWS_SHAPE } from '../../constants';
import CommentsCount from '../Disqus/CommentsCount';

const LatestMediaNewsTab = ({ classes, news, t }) => (
  <div>
    <Link to={NEWS_VIEW.replace(':id', news.id).replace(':slug', news.slug)}>
      <TabContainer>
        <div className={classes.wrapper}>
          <img
            className={classes.image}
            src={process.env.REACT_APP_MEDIA_BASE_URL + (
              news.mainImage ? news.mainImage.url : process.env.REACT_APP_MEDIA_BASE_URL
            )}
            alt="latestMediaNewsPreview"
          />
          <div className={classes.details}>
            <NewsStats
              viewsCount={news.viewsCount}
              commentsCount={(
                news._id && (
                  <CommentsCount
                    identifier={news._id}
                    title={news.title}
                  />
                )
              )}
              createdAt={news.createdAt}
              inline
              classes={{ stats: 'm-l' }}
              t={t}
            />
          </div>
        </div>
      </TabContainer>
      <Typography data-testid="latest-media-news-title" className={classes.title}>
        {news.title}
        <span className={classes.tag}>
          {' '}
                        (
          {news.tags && news.tags[0] ? news.tags[0].name : t('Other')}
                        )
        </span>
      </Typography>
    </Link>
  </div>
);

LatestMediaNewsTab.propTypes = {
  t: PropTypes.func.isRequired,
  news: NEWS_SHAPE.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LatestMediaNewsTab);

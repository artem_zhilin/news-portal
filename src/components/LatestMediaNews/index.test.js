/* global describe, it, expect, t, news, fireEvent, customRender */
import React from 'react';
import LatestMediaNews from './index';

describe('Latest media news', () => {
  it('should render without crashing', () => {
    const { container, getByTestId } = customRender(
      <LatestMediaNews
        t={t}
        latestMediaNews={{ photoNews: news[1], videoNews: news[2] }}
      />,
    );

    expect(getByTestId('latest-media-news-title')).toHaveTextContent(news[1].title);
    expect(getByTestId('photo-tab')).toHaveTextContent('Photo');
    expect(getByTestId('video-tab')).toHaveTextContent('Video');

    expect(container).toMatchSnapshot();
  });

  it('should change tabs properly', () => {
    const { getByTestId } = customRender(
      <LatestMediaNews
        t={t}
        latestMediaNews={{ photoNews: news[1], videoNews: news[2] }}
      />,
    );

    expect(getByTestId('latest-media-news-title')).toHaveTextContent(news[1].title);

    fireEvent.click(getByTestId('video-tab'));

    expect(getByTestId('latest-media-news-title')).toHaveTextContent(news[2].title);
  });
});

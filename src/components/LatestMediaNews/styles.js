const styles = theme => ({
  wrapper: {
    position: 'relative',
    height: '300px',
  },
  image: {
    height: '100%',
    width: '100%',
  },
  details: {
    position: 'absolute',
    bottom: '3%',
    left: '4%',
    color: '#fdfdfd',
    textShadow: '-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
  },
  title: {
    color: theme.palette.common.black,
    fontSize: '1em',
    fontWeight: 600,
  },
  tag: {
    fontSize: '0.85em',
    color: '#9e9e9e',
    fontWeight: 400,
  },
});

export default styles;

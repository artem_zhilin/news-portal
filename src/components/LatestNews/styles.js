const styles = {
  newsTime: {
    fontSize: '1em',
    color: '#9e9e9e',
  },
  newsText: {
    fontSize: '1em',
    fontWeight: 600,
  },
  newsTag: {
    fontSize: '0.85em',
    color: '#9e9e9e',
    fontWeight: 400,
  },
};

export default styles;

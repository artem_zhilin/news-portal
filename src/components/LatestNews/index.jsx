import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Button,
  Typography,
  Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import styles from './styles';
import { DASHBOARD, NEWS_VIEW } from '../../routes';
import SectionTitle from '../SectionTitle';

const LatestNews = ({ classes, t, latestNews }) => (
  <React.Fragment>
    <SectionTitle>{t('Latest news')}</SectionTitle>
    <Grid data-testid="news-container" item>
      {latestNews.map(news => (
        <Grid container key={news._id} justify="space-between" className="m-b-sm">
          <Grid item xs={2}>
            <Typography className={classes.newsTime}>{news.createdAt.format('HH:mm')}</Typography>
          </Grid>
          <Grid item xs={10}>
            <Link to={NEWS_VIEW.replace(':id', news._id).replace(':slug', news.slug)}>
              <Typography className={classes.newsText}>
                {news.title}
                <span className={classes.newsTag}>
                  {' '}
                        (
                  {news.tags[0] ? news.tags[0].name : t('Other')}
                        )
                </span>
                {news.hasPhotos && (
                <i className="material-icons photo-icon">
                  camera_alt
                </i>
                )}
                {news.hasVideo && (
                <i className="material-icons video-icon">
                  videocam
                </i>
                )}
              </Typography>
            </Link>
          </Grid>
        </Grid>
      ))}
    </Grid>
    <Grid item>
      <Link to={DASHBOARD}>
        <Button data-testid="button-all-news" variant="contained" fullWidth color="primary">
          {t('All News')}
        </Button>
      </Link>
    </Grid>
  </React.Fragment>
);

LatestNews.propTypes = {
  classes: PropTypes.object.isRequired,
  latestNews: PropTypes.arrayOf(PropTypes.object).isRequired,
  t: PropTypes.func.isRequired,
};


export default withStyles(styles)(LatestNews);

/* global describe, it, expect, t, customRender, news */
import React from 'react';
import LatestNews from './index';

const newsToRender = news.slice(0, 5);
describe('Latest news', () => {
  it('should render without crashing', () => {
    const { container, getByTestId } = customRender(<LatestNews t={t} latestNews={newsToRender} />);

    expect(container).toMatchSnapshot();
    expect(container).toHaveTextContent('Latest news');
    expect(getByTestId('button-all-news')).toHaveTextContent(/all news/i);
  });

  it('should render news properly', () => {
    const { rerender, getByTestId } = customRender(<LatestNews t={t} latestNews={[]} />);
    expect(getByTestId('news-container').children).toHaveLength(0);
    rerender(<LatestNews t={t} latestNews={newsToRender} />);
    const newsContainer = getByTestId('news-container');
    expect(newsContainer.children).toHaveLength(5);
    expect(newsContainer.children[1]).toHaveTextContent('camera_alt').not.toHaveTextContent('videocam');
    expect(newsContainer.children[2]).toHaveTextContent('videocam').not.toHaveTextContent('camera_alt');
    expect(newsContainer.children[3]).toHaveTextContent('camera_alt').toHaveTextContent('videocam');
  });
});

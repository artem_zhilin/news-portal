const styles = theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    padding: '20px 50px',
    color: '#fff',
    overflow: 'hidden',
    zIndex: theme.zIndex.drawer + 1,
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  footerLink: {
    textDecoration: 'underline',
  },
  ageRule: {
    borderRadius: '50%',
    border: '1px solid #e4e4e485',
    padding: '15px',
    display: 'inline-block',
    color: '#e4e4e485',
    fontSize: '1em',
  },
  copyright: {
    color: '#e4e4e485',
    fontSize: '1em',
  },
});

export default styles;

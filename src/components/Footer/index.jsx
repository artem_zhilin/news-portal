import React from 'react';
import { useTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './styles';
import logo from '../../assets/img/logo_footer.svg';
import { DASHBOARD } from '../../routes';

const Footer = ({ classes }) => {
  const { t } = useTranslation();
  return (
    <footer className={classes.root}>
      <Grid container justify="space-between">
        <Grid item xs={1}>
          <Link to={DASHBOARD}>
            <img className={classes.logo} src={logo} alt="logo-footer" />
          </Link>
        </Grid>
        <Grid item xs={5}>
          <Grid container>
            <Grid item xs={2}>
              <Typography color="inherit" className={classes.ageRule}>
                18+
              </Typography>
            </Grid>
            <Grid item xs={10}>
              <Typography className={classes.copyright}>
                {t('Copyright', { year: (new Date()).getFullYear() })}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Typography color="inherit" variant="subtitle1">
            <Link className={classes.footerLink} to="#">
              {t('Advertisement on site')}
            </Link>
          </Typography>
          <Typography color="inherit" variant="subtitle1">
            <Link className={classes.footerLink} to="#">
              {t('Site rules')}
            </Link>
          </Typography>
        </Grid>
        <Grid item>
          <Typography color="inherit" variant="subtitle1">
              daynews@gmail.com
          </Typography>
          <Typography color="inherit" variant="subtitle1">
              +38&nbsp;097&nbsp;758&nbsp;14&nbsp;54
          </Typography>
        </Grid>
      </Grid>
    </footer>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);

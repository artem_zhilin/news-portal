import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Paper, Typography, Grid } from '@material-ui/core';
import styles from './styles';
import usdSign from '../../assets/img/usd-sign.png';
import euroSign from '../../assets/img/euro-sign.png';
import { CURRENCY_RATES_SHAPE } from '../../constants';

const RatesWidget = ({ classes, rates, t }) => (
  <Paper className={classes.root} elevation={0}>
    <Typography className={classes.text}>
      {t('Currency rates')}
    </Typography>
    <Grid data-testid="rates-container" container justify="space-around" direction="row">
      <Grid item>
        <Grid container direction="column">
          <img className={classes.currencySign} src={usdSign} alt="usd" />
          <Typography className={classes.rate}>{rates.usd}</Typography>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container direction="column">
          <img className={classes.currencySign} src={euroSign} alt="euro" />
          <Typography className={classes.rate}>{rates.euro}</Typography>
        </Grid>
      </Grid>
    </Grid>
    <Typography className={classes.text}>
      {t('Provided by National Bank of Ukraine')}
    </Typography>
  </Paper>
);

RatesWidget.propTypes = {
  classes: PropTypes.object.isRequired,
  rates: CURRENCY_RATES_SHAPE.isRequired,
  t: PropTypes.func.isRequired,
};

export default withStyles(styles)(RatesWidget);

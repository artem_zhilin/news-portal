/* global describe, it, expect, t, customRender */
import React from 'react';
import RatesWidget from './index';

const rates = { usd: '23.64', euro: '28.91' };
describe('Rates widget', () => {
  it('should render without crashing', () => {
    const { container } = customRender(<RatesWidget t={t} rates={rates} />);

    expect(container).toMatchSnapshot();
    expect(container).toHaveTextContent('Currency rates').toHaveTextContent('Provided by National Bank of Ukraine');
  });

  it('should render rates properly', () => {
    const { getByAltText } = customRender(<RatesWidget t={t} rates={rates} />);

    const usdIcon = getByAltText('usd');
    const euroIcon = getByAltText('euro');
    expect(usdIcon.className).toContain('currencySign');
    expect(usdIcon.nextSibling).toHaveTextContent(rates.usd);
    expect(euroIcon.className).toContain('currencySign');
    expect(euroIcon.nextSibling).toHaveTextContent(rates.euro);
  });
});

const styles = theme => ({
  root: {
    background: 'rgb(63, 81, 181, 0.9) url("/static/img/rates-background.jpg") no-repeat center center',
    backgroundSize: 'cover',
    padding: '10px 35px',
  },
  text: {
    color: 'rgba(222, 221, 221, 0.55)',
    fontSize: '1.1em',
    padding: '35px 0',
    letterSpacing: '0.9px',
  },
  rate: {
    color: theme.palette.common.white,
    fontSize: '2.2em',
    marginTop: '5px',
    letterSpacing: '0.9px',
  },
  currencySign: {
    width: '90px',
  },
});

export default styles;

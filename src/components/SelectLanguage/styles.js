const styles = {
  formControl: {
    marginRight: '30px',
    zIndex: 2000,
  },
  icon: {
    display: 'none',
  },
  selectMenu: {
    padding: '6px 5px 7px 5px',
  },
};

export default styles;

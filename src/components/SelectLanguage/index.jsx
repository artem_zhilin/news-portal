import React, { useState } from 'react';
import {
  Select,
  FormControl,
  MenuItem,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import styles from './styles';

const SelectLanguage = ({ classes }) => {
  const [lng, setLng] = useState('en');
  const { i18n } = useTranslation();
  const handleLngChange = ({ target }) => {
    setLng(target.value);
    i18n.changeLanguage(target.value);
  };
  return (
    <FormControl className={classes.formControl}>
      <Select
        classes={{
          root: classes.root,
          icon: classes.icon,
          selectMenu: classes.selectMenu,
        }}
        value={lng}
        onChange={handleLngChange}
        name="lng"
      >
        <MenuItem value="en">EN</MenuItem>
        <MenuItem value="ru">RU</MenuItem>
      </Select>
    </FormControl>
  );
};

SelectLanguage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SelectLanguage);

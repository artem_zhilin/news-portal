import React, { Fragment } from 'react';
import { Typography } from '@material-ui/core';
import moment from 'moment';
import PropTypes from 'prop-types';

const NewsStats = ({
  createdAt, viewsCount, commentsCount, classes, t, inline, iconsSize,
}) => (
  <Fragment>
    <Typography inline={inline} color="inherit" className={classes.date}>
      {moment.isMoment(createdAt) ? (
        `${createdAt.format('DD')} ${t(createdAt.format('MMMM'))} ${createdAt.format('YYYY HH:mm')}`
      ) : (
        createdAt
      )}
    </Typography>
    <Typography inline={inline} color="inherit" className={classes.stats}>
      <i className={`material-icons icon-${iconsSize}`}>
        visibility
      </i>
      &nbsp;
      {viewsCount}
      {' '}
      <i className={`material-icons icon-${iconsSize}`}>
        comment
      </i>
      &nbsp;
      {commentsCount}
    </Typography>
  </Fragment>
);

NewsStats.propTypes = {
  inline: PropTypes.bool,
  classes: PropTypes.object,
  createdAt: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  viewsCount: PropTypes.number,
  commentsCount: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  t: PropTypes.func.isRequired,
  iconsSize: PropTypes.number,
};

NewsStats.defaultProps = {
  inline: false,
  classes: {},
  iconsSize: 20,
  viewsCount: 0,
  commentsCount: 0,
  createdAt: '',
};


export default NewsStats;

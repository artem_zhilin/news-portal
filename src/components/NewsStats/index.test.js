/* global describe, it, expect, t */
import React from 'react';
import { render } from 'react-testing-library';
import moment from 'moment';
import NewsStats from './index';

describe('NewsStats', () => {
  it('should render without crashing', () => {
    const { container } = render(<NewsStats t={t} createdAt={moment('2019-05-12T12:45:00Z')} viewsCount={12} commentsCount={5} />);

    expect(container).toMatchSnapshot();
    expect(container).toHaveTextContent('12 May 2019');
  });
});

/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { insertScript, removeScript, debounce } from '../../utils';
import useDidUpdateEffect from '../../hooks/useDidUpdateEffect';

const queueResetCount = debounce(() => {
  if (window.DISQUSWIDGETS) {
    window.DISQUSWIDGETS.getCount({ reset: true });
  }
}, 300, false);

const CommentsCount = ({
  shortName, url, identifier, title,
}) => {
  const loadInstance = () => {
    const doc = window.document;
    const scriptId = 'dsq-count-scr';
    if (doc.getElementById(scriptId)) {
      queueResetCount();
    } else {
      insertScript(`https://${shortName}.disqus.com/count.js`, scriptId, doc.body);
    }
  };

  const cleanInstance = () => {
    const { body } = window.document;
    removeScript('dsq-count-scr', body);
    window.DISQUSWIDGETS = undefined;
  };

  useEffect(() => {
    loadInstance();
    // eslint-disable-next-line
  }, []);

  useDidUpdateEffect(() => {
    loadInstance();
  }, [title]);

  useDidUpdateEffect(() => {
    cleanInstance();
    loadInstance();
  }, [shortName]);

  return (
    <span
      className="disqus-comment-count"
      data-disqus-identifier={identifier}
      data-disqus-url={url}
    >
        0
    </span>
  );
};

CommentsCount.propTypes = {
  shortName: PropTypes.string,
  url: PropTypes.string,
  identifier: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

CommentsCount.defaultProps = {
  shortName: process.env.REACT_APP_DISQUS_SHORTNAME,
  url: window.location.href,
};

export default CommentsCount;

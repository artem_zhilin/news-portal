/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-undef */
/* eslint-disable func-names */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { insertScript, removeScript } from '../../utils';
import useDidUpdateEffect from '../../hooks/useDidUpdateEffect';

const Disqus = ({
  shortName, url, identifier, title,
}) => {
  const { i18n } = useTranslation();

  const getDisqusConfig = () => function () {
    this.page.identifier = identifier;
    this.page.url = url;
    this.page.title = title;
    this.language = i18n.language;
  };

  const cleanInstance = () => {
    const doc = window.document;
    removeScript('dsq-embed-scr', doc.body);
    if (window && window.DISQUS) {
      window.DISQUS.reset({});
    }

    try {
      delete window.DISQUS;
    } catch (error) {
      window.DISQUS = undefined;
    }
    const disqusThread = doc.getElementById('disqus_thread');
    if (disqusThread) {
      while (disqusThread.hasChildNodes()) {
        disqusThread.removeChild(disqusThread.firstChild);
      }
    }
  };

  const loadInstance = () => {
    const doc = window.document;
    if (window && window.DISQUS && doc.getElementById('dsq-embed-scr')) {
      window.DISQUS.reset({
        reload: true,
        config: getDisqusConfig(),
      });
    } else {
      window.disqus_config = getDisqusConfig();
      window.disqus_shortname = shortName;
      insertScript(`https://${shortName}.disqus.com/embed.js`, 'dsq-embed-scr', doc.body);
    }
  };

  useEffect(() => {
    if (typeof window !== 'undefined' && window.disqus_shortname && window.disqus_shortname !== shortName) {
      cleanInstance();
    }
    loadInstance();
    // eslint-disable-next-line
  }, []);

  useDidUpdateEffect(() => {
    loadInstance();
  }, [i18n.language]);

  useDidUpdateEffect(() => {
    cleanInstance();
    loadInstance();
  }, [shortName]);

  useDidUpdateEffect(() => {
    loadInstance();
  }, [title]);

  return (
    <div id="disqus_thread" />
  );
};

Disqus.propTypes = {
  shortName: PropTypes.string,
  url: PropTypes.string,
  identifier: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

Disqus.defaultProps = {
  shortName: process.env.REACT_APP_DISQUS_SHORTNAME,
  url: window.location.href,
};

export default Disqus;

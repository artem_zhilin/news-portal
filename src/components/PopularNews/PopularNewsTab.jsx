import React from 'react';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import TabContainer from '../TabContainer';
import styles from './styles';
import { NEWS_VIEW } from '../../routes';
import CommentsCount from '../Disqus/CommentsCount';

const PopularNewsTab = ({
  classes, news, displayComments, displayViews,
}) => (
  <TabContainer>
    {news.map(newsItem => (
      <Link key={newsItem._id} to={NEWS_VIEW.replace(':id', newsItem._id).replace(':slug', newsItem.slug)}>
        <Typography className={classes.title}>
          {newsItem.title}
          <span className={classes.views}>
            <i className="material-icons icon-20">
              {displayViews && 'visibility'}
              {displayComments && 'comment'}
            </i>
          &nbsp;
            {displayViews && newsItem.viewsCount}
            {displayComments && (
              <CommentsCount identifier={newsItem._id} title={newsItem.title} />
            )}
          </span>
        </Typography>
      </Link>
    ))}
  </TabContainer>
);

PopularNewsTab.propTypes = {
  news: PropTypes.arrayOf(PropTypes.object).isRequired,
  classes: PropTypes.object.isRequired,
  displayViews: PropTypes.bool,
  displayComments: PropTypes.bool,
};

PopularNewsTab.defaultProps = {
  displayViews: false,
  displayComments: false,
};

export default withStyles(styles)(PopularNewsTab);

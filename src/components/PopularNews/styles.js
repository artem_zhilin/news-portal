const styles = theme => ({
  title: {
    margin: '20px 0',
    fontSize: '1em',
  },
  views: {
    marginLeft: '5px',
    fontSize: '0.9em',
    color: '#989898',
  },
});

export default styles;

import React, { useState } from 'react';
import {
  Tabs,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import SectionTitle from '../SectionTitle';
import TabStyled from '../TabStyled';
import PopularNewsTab from './PopularNewsTab';

const PopularNews = ({ t, popularNews }) => {
  const [tabValue, setTabValue] = useState('views');

  const handleTabChange = (e, value) => {
    setTabValue(value);
  };

  return (
    <React.Fragment>
      <SectionTitle>{t('Most popular')}</SectionTitle>
      <Tabs indicatorColor="primary" value={tabValue} onChange={handleTabChange}>
        <TabStyled value="views" label={t('Views')} />
        <TabStyled value="comments" label={t('Comments')} />
      </Tabs>
      {tabValue === 'views' && <PopularNewsTab displayViews news={popularNews.views} />}
      {tabValue === 'comments' && <PopularNewsTab displayComments news={popularNews.comments} />}
    </React.Fragment>
  );
};

PopularNews.propTypes = {
  popularNews: PropTypes.shape({
    views: PropTypes.arrayOf(PropTypes.object),
    comments: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  t: PropTypes.func.isRequired,
};


export default PopularNews;

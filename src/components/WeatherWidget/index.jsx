/* eslint-disable spellcheck/spell-checker */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import useDidUpdateEffect from '../../hooks/useDidUpdateEffect';
import { insertScript } from '../../utils';

const WeatherWidget = ({ t, i18n }) => {
  useEffect(() => {
    const scriptId = 'weather-widget-scr';
    if (!window.document.getElementById(scriptId)) {
      insertScript('https://weatherwidget.io/js/widget.min.js', scriptId, window.document.body);
    } else if (window.__weatherwidget_init) {
      window.__weatherwidget_init();
    }
  }, []);

  useDidUpdateEffect(() => {
    window.__weatherwidget_init();
  }, [i18n.language]);

  return (
    <div style={{
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      minHeight: 211,
    }}
    >
      <a
        className="weatherwidget-io"
        href={`https://forecast7.com/${i18n.language}/46d6432d62/kherson/`}
        data-label_1={t('Kherson')}
        data-label_2={t('Weather')}
        data-days="7"
        data-theme="weather_one"
      >
        {t('Kherson')}
        {t('Weather')}
      </a>
    </div>
  );
};

WeatherWidget.propTypes = {
  i18n: PropTypes.object.isRequired,
  t: PropTypes.func.isRequired,
};

export default WeatherWidget;

const styles = theme => ({
  wrapper: {
    position: 'relative',
    height: '400px',
    [theme.breakpoints.down('md')]: {
      height: '100%',
      width: '100%',
    },
  },
  image: {
    height: '100%',
    [theme.breakpoints.down('md')]: {
      height: 'auto',
      width: '100%',
    },
  },
  details: {
    position: 'absolute',
    left: '3%',
    bottom: '5%',
    color: theme.palette.common.white,
    textAlign: 'left',
  },
  slideWrapper: {
    position: 'relative',
  },
  date: {
    fontSize: '0.8em',
    textShadow: '-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
  },
  stats: {
    fontSize: '0.8em',
    textShadow: '-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
  },
  title: {
    fontWeight: 600,
    fontSize: '1em',
    textShadow: '-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000',
  },
  navigation: {
    position: 'absolute',
    color: theme.palette.common.white,
    right: '4%',
    bottom: '4%',
  },
  navButton: {
    padding: 0,
    minWidth: 0,
    color: theme.palette.common.white,
  },
});

export default styles;

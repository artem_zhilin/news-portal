import React from 'react';
import { Button, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import styles from './styles';
import { NEWS_VIEW } from '../../routes';
import CarouselStyled from '../CarouselStyled';
import useCarousel from '../../hooks/useCarousel';
import NewsStats from '../NewsStats';
import { NEWS_SHAPE } from '../../constants';
import CommentsCount from '../Disqus/CommentsCount';

const NewsCarousel = ({
  carouselNews, classes, t, auto,
}) => {
  const [setSwipeElement, handleSlideChange, currentSlide] = useCarousel(auto);

  return (
    <div className={classes.wrapper}>
      <CarouselStyled
        childCount={carouselNews.length}
        refFunc={(el) => { setSwipeElement(el); }}
      >
        {carouselNews.map(news => (
          <div className={classes.slideWrapper} key={news._id}>
            <img
              className={classes.image}
              src={process.env.REACT_APP_MEDIA_BASE_URL + news.mainImage.url}
              alt="news-img"
            />
            <div className={classes.details}>
              <Link to={NEWS_VIEW.replace(':id', news._id).replace(':slug', news.slug)}>
                <Typography color="inherit" className={classes.title}>
                  {news.title}
                </Typography>
                <NewsStats
                  viewsCount={news.viewsCount}
                  commentsCount={(
                    <CommentsCount
                      identifier={news._id}
                      title={news.title}
                    />
                  )}
                  createdAt={news.createdAt}
                  inline
                  iconsSize={15}
                  classes={{ date: `${classes.date} m-r-sm`, stats: classes.stats }}
                  t={t}
                />
              </Link>
            </div>
          </div>
        ))}
      </CarouselStyled>
      <div className={classes.navigation}>
        <Typography className="m-r-sm" color="inherit" inline>
          {`${currentSlide} ${t('of')} ${carouselNews.length}`}
        </Typography>
        <Button className={classes.navButton} onClick={() => handleSlideChange('prev')}>
          <i className="material-icons">
          arrow_back_ios
          </i>
        </Button>
        <Button className={classes.navButton} onClick={() => handleSlideChange('next')}>
          <i className="material-icons">
          arrow_forward_ios
          </i>
        </Button>
      </div>
    </div>
  );
};

NewsCarousel.propTypes = {
  classes: PropTypes.object.isRequired,
  carouselNews: PropTypes.arrayOf(NEWS_SHAPE).isRequired,
  t: PropTypes.func.isRequired,
  auto: PropTypes.number,
};

NewsCarousel.defaultProps = {
  auto: 0,
};

export default withStyles(styles)(NewsCarousel);

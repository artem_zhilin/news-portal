import React from 'react';
import PropTypes from 'prop-types';
import ReactSwipe from 'react-swipe';

export const styles = {
  container: {
    overflow: 'hidden',
    visibility: 'hidden',
    position: 'relative',
    height: '100%',
  },
  wrapper: {
    overflow: 'hidden',
    position: 'relative',
    height: '100%',
  },
  child: {
    float: 'left',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    position: 'relative',
    transitionProperty: 'transform',
    background: 'linear-gradient(0deg, rgba(136,136,136,1) 0%, rgba(255,255,255,1) 35%)',
  },
};

const CarouselStyled = ({
  childCount, refFunc, className, style, swipeOptions, children,
}) => (
  <ReactSwipe
    className={className}
    childCount={childCount}
    style={style}
    swipeOptions={swipeOptions}
    ref={refFunc}
  >
    {children}
  </ReactSwipe>
);

CarouselStyled.propTypes = {
  childCount: PropTypes.number.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  swipeOptions: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  refFunc: PropTypes.func.isRequired,
};

CarouselStyled.defaultProps = {
  className: 'carousel',
  style: styles,
  swipeOptions: { continuous: true },
};

export default CarouselStyled;

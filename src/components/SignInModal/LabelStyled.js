import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';

const LabelStyled = withStyles({
  marginDense: {
    transform: 'translate(10px, 26px) scale(1)',
  },
  shrink: {
    transform: 'translate(0, 1.5px) scale(0.75) !important',
  },
})(InputLabel);

export default LabelStyled;

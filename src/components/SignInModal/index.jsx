import React, { useState } from 'react';
import {
  DialogContent,
  Dialog,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  Typography,
  FormControlLabel,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import InputStyled from './InputStyled';
import LabelStyled from './LabelStyled';
import CheckboxStyled from './CheckboxStyled';
import styles from './styles';
import facebook from '../../assets/img/facebook.png';
import google from '../../assets/img/google.png';

const SocialIcons = () => (
  <div className="text-center m-t-xs">
    <Link className="m-r-xs" to="#">
      <img width={30} src={facebook} alt="facebook" />
    </Link>
    <Link to="#">
      <img width={30} src={google} alt="google" />
    </Link>
  </div>
);

const SignInModal = ({
  classes, opened, handleClose, t, errors,
}) => {
  const userAgreementText = t('I agree to the terms of use and processing of my personal data');

  const [signInData, setSignInData] = useState({
    email: '',
    password: '',
  });

  const [signUpData, setSignUpData] = useState({
    email: '',
    password: '',
    name: '',
  });

  const handleInputChange = ({ target: { name, value } }) => {
    const signIn = 'SignIn';
    const signUp = 'SignUp';
    if (name.indexOf(signIn) !== -1) {
      name = name.replace(signIn, '');
      setSignInData({ ...signInData, [name]: value });
    } else if (name.indexOf(signUp) !== -1) {
      name = name.replace(signUp, '');
      setSignUpData({ ...signUpData, [name]: value });
    }
  };

  const [rememberMe, setRememberMe] = useState(true);
  const handleRememberChange = ({ target: { checked } }) => {
    setRememberMe(checked);
  };

  const [userAgreement, setUserAgreement] = useState(true);
  const handleUserAgreementChange = ({ target: { checked } }) => {
    setUserAgreement(checked);
  };

  return (
    <Dialog
      open={opened}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      maxWidth={false}
      classes={{ paper: classes.paper }}
    >
      <DialogContent className={classes.dialogContent}>
        <Grid container>
          <Grid item lg={6} md={6} sm={12} xs={12}>
            <div className={classes.formWrapper}>
              <Typography className={classes.formTitle}>
                {t('Sign In')}
              </Typography>
              <form onSubmit={(e) => { e.preventDefault(); }}>
                <div className="m-b-md">
                  <FormControl error={!!errors.email} margin="dense" required fullWidth>
                    <LabelStyled htmlFor="emailSignIn">{t('Email')}</LabelStyled>
                    <InputStyled
                      disableUnderline
                      onChange={handleInputChange}
                      value={signInData.email}
                      id="emailSignIn"
                      name="emailSignIn"
                      autoComplete="email"
                    />
                    {errors.email && (
                    <FormHelperText>{errors.email}</FormHelperText>
                    )}
                  </FormControl>
                  <FormControl error={!!errors.password} margin="dense" required fullWidth>
                    <LabelStyled htmlFor="passwordSignIn">{t('Password')}</LabelStyled>
                    <InputStyled
                      disableUnderline
                      onChange={handleInputChange}
                      value={signInData.password}
                      id="passwordSignIn"
                      name="passwordSignIn"
                      type="password"
                    />
                    <FormHelperText className="text-right">
                      <Link to="#">
                        {t('Forgot password')}
                      </Link>
                    </FormHelperText>
                  </FormControl>
                </div>
                <Button className={classes.button} variant="contained" color="primary" fullWidth>{t('Sign in')}</Button>
                <FormControlLabel
                  control={(
                    <CheckboxStyled
                      checked={rememberMe}
                      onChange={handleRememberChange}
                      value="rememberMe"
                    />
                  )}
                  label={t('Remember me')}
                />
                <Typography align="center">
                  {t('or sign in via social networks')}
                </Typography>
                <SocialIcons />
              </form>
            </div>
          </Grid>
          <Grid item lg={6} md={6} sm={12} xs={12} className={classes.leftDivider}>
            <div className={classes.formWrapper}>
              <Typography className={classes.formTitle}>
                {t('Sign Up')}
              </Typography>
              <form onSubmit={(e) => { e.preventDefault(); }}>
                <div className="m-b-md">
                  <FormControl error={!!errors.email} margin="dense" required fullWidth>
                    <LabelStyled htmlFor="emailSignUp">{t('Email')}</LabelStyled>
                    <InputStyled
                      disableUnderline
                      onChange={handleInputChange}
                      value={signUpData.email}
                      id="emailSignUp"
                      name="emailSignUp"
                      autoComplete="email"
                    />
                    {errors.email && (
                    <FormHelperText>{errors.email}</FormHelperText>
                    )}
                  </FormControl>
                  <FormControl error={!!errors.name} margin="dense" required fullWidth>
                    <LabelStyled htmlFor="nameSignUp">{t('Name')}</LabelStyled>
                    <InputStyled
                      disableUnderline
                      onChange={handleInputChange}
                      value={signUpData.name}
                      id="nameSignUp"
                      name="nameSignUp"
                    />
                    {errors.name && (
                    <FormHelperText>{errors.name}</FormHelperText>
                    )}
                  </FormControl>
                  <FormControl error={!!errors.password} margin="dense" required fullWidth>
                    <LabelStyled htmlFor="passwordSignUp">{t('Password')}</LabelStyled>
                    <InputStyled
                      disableUnderline
                      onChange={handleInputChange}
                      value={signUpData.password}
                      id="passwordSignUp"
                      name="passwordSignUp"
                      type="password"
                    />
                  </FormControl>
                </div>
                <Button className={classes.button} variant="contained" color="primary" fullWidth>{t('Sign up')}</Button>
                <FormControlLabel
                  required
                  control={(
                    <CheckboxStyled
                      checked={userAgreement}
                      onChange={handleUserAgreementChange}
                      value="userAgreement"
                    />
                  )}
                  label={(
                    <Typography className={classes.userAgreement}>
                      {userAgreementText.split('_terms_')[0]}
                      <Link className={classes.userAgreementLink} to="#">
                        {t('the terms of use')}
                      </Link>
                      {userAgreementText.split('_terms_')[1]}
                    </Typography>
                  )}
                />
                <Typography align="center">
                  {t('or sign up via social networks')}
                </Typography>
                <SocialIcons />
              </form>
            </div>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};

SignInModal.propTypes = {
  opened: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  errors: PropTypes.object,
};

SignInModal.defaultProps = {
  errors: {},
};

export default withStyles(styles)(SignInModal);

import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';

const InputStyled = withStyles({
  root: {
    backgroundColor: 'rgba(57, 73, 160, 0.1)',
    borderRadius: '3px',
  },
  input: {
    padding: '10px',
  },
})(Input);

export default InputStyled;

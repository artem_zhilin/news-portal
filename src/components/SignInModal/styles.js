const styles = theme => ({
  paper: {
    maxWidth: '750px',
  },
  dialogContent: {
    padding: '40px 0',
    '&:first-child': {
      paddingTop: '40px',
    },
  },
  formWrapper: {
    padding: '0 45px',
  },
  formTitle: {
    color: theme.palette.common.black,
    fontWeight: 600,
    fontSize: '1.1em',
  },
  button: {
    padding: '9px 16px',
    textTransform: 'none',
    letterSpacing: '0.5px',
  },
  userAgreement: {
    textAlign: 'center',
    color: '#9a9a9a',
    fontSize: '0.85em',
    margin: '15px 0',
  },
  userAgreementLink: {
    textDecoration: 'underline',
  },
  leftDivider: {
    borderLeft: '1px solid #e9e9e9',
  },
});

export default styles;

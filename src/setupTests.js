/* eslint-disable spellcheck/spell-checker */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-unused-vars */
/* eslint-disable import/no-extraneous-dependencies */
/* global jest */
import React from 'react';
import 'jest-dom/extend-expect';
import 'jest-chain';
import 'react-testing-library/cleanup-after-each';
import { render, fireEvent } from 'react-testing-library';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import moment from 'moment';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import './i18n';
import reducer from './reducers';

const customRender = (
  ui,
  options,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {},
  {
    initialState = {},
    store = createStore(reducer, initialState, applyMiddleware(thunk)),
  } = {},
) => {
  const rendered = render(
    (
      <Provider store={store}>
        <Router history={history}>{ui}</Router>
      </Provider>
    ),
    options,
  );
  return {
    ...rendered,
    rerender: (uiR, optionsR) => (
      customRender(uiR, { container: rendered.container, ...optionsR }, { history })
    ),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history,
  };
};

global.customRender = customRender;

global.fireEvent = fireEvent;

global.t = jest.fn().mockImplementation(value => value);

global.news = [
  {
    _id: '1',
    title: 'Заголовок первой новости без медиа файлов еще чуть текста',
    viewsCount: 12,
    commentsCount: 3,
    createdAt: moment('2019-05-19T16:34:34Z'),
    tags: [{ name: 'Экономика' }],
    hasPhotos: false,
    hasVideo: false,
    photos: [],
    mainImage: { url: '/uploads/496cf5a2975d4d1ca3ec1ebb84a53b15.jpg' },
    video: null,
  },
  {
    _id: '2',
    title: 'Заголовок второй новости без видео но с фото еще чуть текста',
    viewsCount: 2,
    commentsCount: 0,
    createdAt: moment('2019-05-19T12:20:34Z'),
    tags: [{ name: 'Политика' }],
    hasPhotos: true,
    hasVideo: false,
    photos: [{ url: 'asd' }],
    mainImage: { url: '/uploads/811fd611953d4d4684031f04e1d32a45.jpg' },
    video: null,
  },
  {
    _id: '3',
    title: 'Заголовок третей новости без фото но с видео еще чуть текста и еще чуть текста и еще',
    viewsCount: 0,
    commentsCount: 0,
    createdAt: moment('2019-05-18T10:15:34Z'),
    tags: [{ name: 'Авто' }],
    mainImage: { url: '/uploads/c7cc3c3f7a044729947c5c772a58cde1.jpg' },
    photos: [],
    video: { url: 'asd' },
    hasPhotos: false,
    hasVideo: true,
  },
  {
    _id: '4',
    title: 'Заголовок четвертой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
    viewsCount: 20,
    commentsCount: 5,
    mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
    createdAt: moment('2019-05-18T07:05:34Z'),
    tags: [{ name: 'Украина' }],
    photos: [1],
    video: 'asd',
    hasPhotos: true,
    hasVideo: true,
  },
  {
    _id: '5',
    title: 'Заголовок пятой новости в котором будет много текста много текста много текста много текста еще много текста еще больше текста',
    viewsCount: 10,
    commentsCount: 2,
    createdAt: moment('2019-05-18T06:49:34Z'),
    tags: [{ name: 'Бизнес' }],
    mainImage: { url: '/uploads/3a236d156c53463ba3e9f48e1b1df387.jpg' },
    photos: [],
    video: null,
    hasPhotos: false,
    hasVideo: false,
  },
  {
    _id: '6',
    title: 'Заголовок шестой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
    viewsCount: 88,
    commentsCount: 28,
    mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
    createdAt: moment('2019-05-20T17:05:34Z'),
    tags: [{ name: 'Украина' }],
    photos: [1],
    video: 'asd',
    hasPhotos: true,
    hasVideo: true,
  },
  {
    _id: '7',
    title: 'Заголовок седьмой новости с фото и с видео еще чуть текста и еще чуть текста и еще',
    viewsCount: 91,
    commentsCount: 21,
    mainImage: { url: '/uploads/51d3a9c8bbcd4650ad1e517c21e8172e.jpg' },
    createdAt: moment('2019-05-20T17:05:34Z'),
    tags: [{ name: 'Украина' }],
    photos: [1],
    video: 'asd',
    hasPhotos: true,
    hasVideo: true,
  },
  {
    _id: '8',
    title: 'Заголовок восьмой новости без фото но с видео еще чуть текста и еще чуть текста и еще',
    viewsCount: 0,
    commentsCount: 0,
    createdAt: moment('2019-05-15T19:20:34Z'),
    tags: [{ name: 'Авто' }],
    mainImage: { url: '/uploads/c7cc3c3f7a044729947c5c772a58cde1.jpg' },
    photos: [],
    video: 'asd',
    hasPhotos: false,
    hasVideo: true,
  },
  {
    _id: '9',
    title: 'Заголовок девятой новости без видео но с фото еще чуть текста',
    viewsCount: 2,
    commentsCount: 0,
    createdAt: moment('2019-05-19T10:20:34Z'),
    tags: [{ name: 'Политика' }],
    photos: [1],
    mainImage: { url: '/uploads/811fd611953d4d4684031f04e1d32a45.jpg' },
    video: null,
    hasPhotos: true,
    hasVideo: false,
  },
];

global.rate = 25.45;

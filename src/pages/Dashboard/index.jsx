import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import {
  fetchEditorChoiceNews,
  fetchPopularNews,
} from '../../actions/newsActions';
import EditorChoiceNews from '../../components/EditorChoiceNews';
import {
  getEditorChoiceNews,
  getLatestNews,
  getPopularNews,
} from '../../selectors/newsSelectors';
import PopularNews from '../../components/PopularNews';
import NewsCarousel from '../../components/NewsCarousel';
import Loader from '../../components/Loader';

const Dashboard = ({
  fetchEditorChoiceNewsDispatch,
  fetchPopularNewsDispatch,
  latestNews,
  editorChoiceNews,
  popularNews,
}) => {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      await fetchEditorChoiceNewsDispatch();
      await fetchPopularNewsDispatch();
      setIsLoading(false);
    })();
    // eslint-disable-next-line
  }, []);

  return (
    isLoading ? (
      <Loader />
    ) : (
      <Grid container direction="row">
        <Grid className="m-b" item xs={12}>
          <NewsCarousel t={t} auto={0} carouselNews={latestNews} />
        </Grid>
        <Grid className="m-b" item xs={12}>
          <EditorChoiceNews editorChoiceNews={editorChoiceNews} t={t} />
        </Grid>
        <Grid className="m-b" item xs={12}>
          <PopularNews popularNews={popularNews} t={t} />
        </Grid>
      </Grid>
    )
  );
};

Dashboard.propTypes = {
  latestNews: PropTypes.arrayOf(PropTypes.object).isRequired,
  editorChoiceNews: PropTypes.arrayOf(PropTypes.object).isRequired,
  popularNews: PropTypes.shape({
    views: PropTypes.arrayOf(PropTypes.object),
    comments: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  fetchEditorChoiceNewsDispatch: PropTypes.func.isRequired,
  fetchPopularNewsDispatch: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  fetchEditorChoiceNewsDispatch: fetchEditorChoiceNews,
  fetchPopularNewsDispatch: fetchPopularNews,
};

const mapStateToProps = state => ({
  latestNews: getLatestNews(state),
  editorChoiceNews: getEditorChoiceNews(state),
  popularNews: getPopularNews(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

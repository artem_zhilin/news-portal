const bodyImageMaxHeight = 450;
const carouselImageHeight = 400;

const styles = theme => ({
  title: {
    fontWeight: 600,
  },
  mediaWrapper: {
    maxHeight: '600px',
    textAlign: 'center',
  },
  bodyImageWrapper: {
    textAlign: 'center',
    maxHeight: `${bodyImageMaxHeight}px`,
    [theme.breakpoints.down('md')]: {
      maxHeight: 'none',
      width: '100%',
    },
  },
  bodyImage: {
    maxHeight: `${bodyImageMaxHeight}px`,
    width: 'auto',
    [theme.breakpoints.down('md')]: {
      maxHeight: 'none',
      width: '100%',
    },
  },
  media: {
    width: '100%',
    margin: '20px 0',
  },
  date: {
    color: '#757575',
    fontSize: '0.9em',
  },
  stats: {
    color: '#757575',
    fontSize: '0.9em',
    marginLeft: '20px',
  },
  carouselWrapper: {
    position: 'relative',
    marginTop: '15px',
  },
  slideWrapper: {
    maxHeight: `${carouselImageHeight}px`,
    [theme.breakpoints.down('md')]: {
      maxHeight: 'none',
      width: '100%',
    },
  },
  slideImage: {
    maxHeight: `${carouselImageHeight}px`,
    width: 'auto',
    [theme.breakpoints.down('md')]: {
      maxHeight: 'none',
      width: '100%',
    },
  },
  slideButton: {
    position: 'absolute',
    padding: 0,
    minWidth: 0,
    height: '100%',
    width: '50px',
    '&:first-child': {
      left: 0,
      top: 0,
    },
    '&:nth-child(2)': {
      right: 0,
      top: 0,
    },
  },
});

export default styles;

import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import {
  Grid, Typography, Button, Chip,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import Loader from '../../components/Loader';
import Disqus from '../../components/Disqus';
import styles from './styles';
import { getNews } from '../../selectors/newsSelectors';
import { fetchNews } from '../../actions/newsActions';
import useDidUpdateEffect from '../../hooks/useDidUpdateEffect';
import NewsStats from '../../components/NewsStats';
import CarouselStyled from '../../components/CarouselStyled';
import useCarousel from '../../hooks/useCarousel';
import { NEWS_SHAPE } from '../../constants';
import CommentsCount from '../../components/Disqus/CommentsCount';

const News = ({
  classes,
  fetchNewsDispatch,
  news,
  match: { params },
}) => {
  const { t } = useTranslation();
  const [formattedBody, setFormattedBody] = useState(news.body);
  const [setSwipeElement, handleSlideChange] = useCarousel();
  const [isLoading, setIsLoading] = useState(true);

  // Transform body from markup to html
  const formatBody = (body) => {
    if (body) {
      let tempBody = body;
      // Find all embedded images
      const imagesLinks = tempBody.match(/!\[.*\]\(http.*(jpg|jpeg)\)/gm);
      if (imagesLinks) {
        imagesLinks.forEach((link) => {
          // Get image src and alt
          let alt = link.match(/!\[(.*)\]/);
          alt = alt && alt[1] ? alt[1] : 'text';
          let src = link.match(/\((.*)\)/);
          src = src && src[1] ? src[1] : false;
          // Insert image instead of markup only if it is not displayed as main image
          const position = tempBody.indexOf(link);
          tempBody = tempBody.replace(link, '');
          if (src && (src !== news.mainImage || news.hasVideo)) {
            const image = `<div class="${classes.bodyImageWrapper}"><img class="${classes.bodyImage}" src="${src}" alt="${alt}" /></div>`;
            tempBody = tempBody.substr(0, position) + image + tempBody.substr(position);
          }
        });
      }
      tempBody = tempBody.replace(/(<br>){3,}/gm, '<br><br>');
      tempBody = tempBody.replace(/^(<br>){1,}/gm, '');
      return tempBody;
    }
    return body;
  };

  useEffect(() => {
    setIsLoading(true);
    fetchNewsDispatch(params.id).then(() => {
      setIsLoading(false);
    });
    // eslint-disable-next-line
  }, [params.id]);

  useDidUpdateEffect(() => {
    setFormattedBody(formatBody(news.body));
  }, [news]);

  return (
    isLoading ? (
      <Loader />
    ) : (
      <Grid container>
        <Grid item xs={12} md={12} sm={12} lg={12}>
          <Typography variant="h6" className={classes.title}>
            {news.title}
          </Typography>
          <NewsStats
            viewsCount={news.viewsCount}
            commentsCount={(
            news._id && (
              <CommentsCount
                identifier={news._id}
                title={news.title}
              />
            )
          )}
            createdAt={news.createdAt}
            inline
            classes={{ date: classes.date, stats: classes.stats }}
            t={t}
          />
        </Grid>
        <Grid className={classes.mediaWrapper} item xs={12} md={12} sm={12} lg={12}>
          {news.hasVideo ? (
            <video key={news._id} className={classes.media} controls>
              <source
                src={process.env.REACT_APP_MEDIA_BASE_URL + news.video.url}
                type={news.video.mime}
              />
            </video>
          ) : (
            <img src={process.env.REACT_APP_MEDIA_BASE_URL + news.mainImage.url} className={classes.media} alt="news-main" />
          )}
        </Grid>
        <Grid item xs={12} md={12} sm={12} lg={12}>
          <Typography component="div" variant="subtitle1" data-testid="news-body" dangerouslySetInnerHTML={{ __html: formattedBody }} />
        </Grid>
        {news.hasPhotos && (
        <Grid item xs={12} md={12} sm={12} lg={12}>
          <div className={classes.carouselWrapper}>
            <CarouselStyled
              childCount={news.photos.length}
              refFunc={(el) => { setSwipeElement(el); }}
            >
              {news.photos.map(photo => (
                <div className={classes.slideWrapper} key={photo._id}>
                  <img className={classes.slideImage} src={process.env.REACT_APP_MEDIA_BASE_URL + photo.url} alt="news-carousel-img" />
                </div>
              ))}
            </CarouselStyled>
            <div>
              <Button className={classes.slideButton} onClick={() => handleSlideChange('prev')}>
                <i className="material-icons">
              arrow_back_ios
                </i>
              </Button>
              <Button className={classes.slideButton} onClick={() => handleSlideChange('next')}>
                <i className="material-icons">
              arrow_forward_ios
                </i>
              </Button>
            </div>
          </div>
        </Grid>
        )}
        <Grid className="m-t" item xs={12} md={12} sm={12} lg={12}>
          <Typography inline variant="subtitle1">
            <b>{`${t('Tags')}:`}</b>
          </Typography>
          {news.tags.map(tag => (
            <Chip key={tag._id} className="m-sides-xs" label={tag.name} />
          ))}
        </Grid>
        {news._id && (
        <Grid className="m-t" item xs={12} md={12} sm={12} lg={12}>
          <Disqus
            shortName={process.env.REACT_APP_DISQUS_SHORTNAME}
            title={news.title}
            identifier={news._id}
          />
        </Grid>
        )}
      </Grid>
    )
  );
};

News.propTypes = {
  match: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  news: NEWS_SHAPE.isRequired,
  fetchNewsDispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  news: getNews(state),
});

const mapDispatchToProps = {
  fetchNewsDispatch: fetchNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(News));

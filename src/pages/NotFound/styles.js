const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: '#3c4858',
    },
  },
  backLink: {
    textDecoration: 'underline',
    color: '#fff',
    '&:hover': {
      color: '#fff',
    },
    '&:active': {
      color: '#fff',
    },
    '&:visited': {
      color: '#fff',
    },
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    minHeight: '100vh',
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  page: {
    border: 0,
    margin: 0,
    color: '#fff',
    padding: '120px 0',
    alignItems: 'center',
    position: 'relative',
    height: '100%',
    display: 'flex!important',
  },
  notFoundWrapper: {
    zIndex: 3,
    color: '#fff',
    width: '100%',
    padding: '0 15px',
    textAlign: 'center',
  },
  flexWrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    boxSizing: 'border-box',
  },
  textWrapper: {
    padding: '0 15px !important',
    flexGrow: 0,
    maxWidth: '100%',
    flexBasis: '100%',
    margin: '0',
  },
  text404: {
    fontSize: '12.7em',
    margin: '25px 0',
    letterSpacing: '14px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
  },
  textNotFound: {
    fontSize: '2.0em',
    marginTop: 0,
    marginBottom: '8px',
    fontWeight: 300,
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
  },
  helperText: {
    fontSize: '1.3em',
    marginTop: 0,
    fontWeight: 300,
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
  },
});

export default styles;

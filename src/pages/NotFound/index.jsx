import React, { Fragment } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { DASHBOARD } from '../../routes/index';
import styles from './styles';

const NotFound = ({ classes }) => (
  <Fragment>
    <CssBaseline />
    <main className={classes.layout}>
      <div className={classes.page}>
        <div className={classes.notFoundWrapper}>
          <h1 className={classes.text404}>404</h1>
          <h3 className={classes.textNotFound}>Page not found :(</h3>
          <h4 className={classes.helperText}>Ooooups! Looks like you got lost.</h4>
          <Link className={classes.backLink} to={DASHBOARD}>Back to app</Link>
        </div>
      </div>
    </main>
  </Fragment>
);

NotFound.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NotFound);

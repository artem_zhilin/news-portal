/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import moment from 'moment';
import { FETCH_CURRENCY_RATES } from '../types/commonTypes';

export const fetchCurrencyRates = () => (dispatch) => {
  // eslint-disable-next-line spellcheck/spell-checker
  const url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange';
  return (
    axios
      .all([
        axios(url, { params: { date: moment().format('YYYYMMDD'), valcode: 'USD', json: 'true' } }),
        axios(url, { params: { date: moment().format('YYYYMMDD'), valcode: 'EUR', json: 'true' } }),
      ])
      .then(([usdResponse, euroResponse]) => {
        const { data: [usdRate] } = usdResponse;
        const { data: [euroRate] } = euroResponse;
        const rates = { usd: usdRate.rate.toFixed(2), euro: euroRate.rate.toFixed(2) };
        dispatch({
          type: FETCH_CURRENCY_RATES,
          payload: rates,
        });
      })
      .catch((error) => {
        console.log(error);
        return false;
      })
  );
};

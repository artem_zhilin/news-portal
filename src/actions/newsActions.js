import axios from 'axios';
import {
  FETCH_LATEST_NEWS,
  FETCH_EDITOR_CHOICE_NEWS,
  FETCH_NEWS,
  FETCH_POPULAR_NEWS,
  FETCH_LATEST_MEDIA_NEWS,
} from '../types/newsTypes';
import { transformDates, transformNews, transformInParams } from '../utils';

export const fetchDashboardNews = () => dispatch => (
  axios
    .all([
      axios('/news', { ...transformDates, params: { _limit: 5, _sort: 'createdAt:desc' } }),
      axios('/news', { ...transformDates, params: { _limit: 6, _sort: '_id:asc' } }),
    ])
    .then(axios.spread((latestNewsResponse, editorChoiceResponse) => {
      const { data: latestNews } = latestNewsResponse;
      const { data: editorChoiceNews } = editorChoiceResponse;
      dispatch({
        type: FETCH_LATEST_NEWS,
        payload: latestNews,
      });
      dispatch({
        type: FETCH_EDITOR_CHOICE_NEWS,
        payload: editorChoiceNews,
      });
    }))
    .catch(({ response }) => {
      if (response) {
        console.log(response);
      }
      return false;
    })
);

export const fetchLatestNews = () => dispatch => (
  axios('/news', { ...transformDates, params: { _limit: 5, _sort: 'createdAt:desc' } })
    .then(({ data: latestNews }) => {
      dispatch({
        type: FETCH_LATEST_NEWS,
        payload: latestNews,
      });
      return true;
    })
    .catch(({ response }) => {
      if (response) {
        console.log(response);
      }
      return false;
    })
);

export const fetchEditorChoiceNews = () => dispatch => (
  axios('/news', { ...transformDates, params: { _limit: 6, _sort: '_id:asc' } })
    .then(({ data: editorChoiceNews }) => {
      dispatch({
        type: FETCH_EDITOR_CHOICE_NEWS,
        payload: editorChoiceNews,
      });
      return true;
    })
    .catch(({ response }) => {
      if (response) {
        console.log(response);
      }
      return false;
    })
);

export const fetchPopularNews = () => dispatch => (
  axios(`${process.env.REACT_APP_DISQUS_API_BASE_URL}/threads/listPopular.json`,
    {
      params: {
        limit: 5, api_key: process.env.REACT_APP_DISQUS_API_KEY, interval: '7d', forum: process.env.REACT_APP_DISQUS_SHORTNAME,
      },
    })
    .then(({ data: { response } }) => {
      axios
        .all([
          axios('/news', { ...transformDates, params: { _limit: 5, _sort: 'viewsCount:desc,createdAt:desc' } }),
          axios(`/news${transformInParams('_id', response.map(({ identifiers }) => identifiers[0]), transformDates)}`),
        ])
        .then(([popularByViewsResponse, popularByCommentsResponse]) => {
          const { data: views } = popularByViewsResponse;
          const { data: comments } = popularByCommentsResponse;
          dispatch({
            type: FETCH_POPULAR_NEWS,
            payload: { views, comments },
          });
        })
        .catch((error) => {
          console.log(error);
          return false;
        });
    })
    .catch((error) => {
      console.log(error);
      return false;
    })
);

export const fetchLatestMediaNews = () => dispatch => (
  axios
    .all([
      axios('/news', { ...transformDates, params: { hasPhotos: true, _limit: 1, _sort: 'createdAt:desc' } }),
      axios('/news', { ...transformDates, params: { hasVideo: true, _limit: 1, _sort: 'createdAt:desc' } }),
    ])
    .then(([photoNewsResponse, videoNewsResponse]) => {
      const { data: photoNews } = photoNewsResponse;
      const { data: videoNews } = videoNewsResponse;
      const news = {};
      if (photoNews[0]) {
        [news.photoNews] = photoNews;
      }
      if (videoNews[0]) {
        [news.videoNews] = videoNews;
      }
      dispatch({
        type: FETCH_LATEST_MEDIA_NEWS,
        payload: news,
      });
    })
    .catch(({ response }) => {
      if (response) {
        console.log(response);
      }
      return false;
    })
);

export const fetchNews = newsId => dispatch => (
  axios(`/news/${newsId}`, transformNews)
    .then(({ data, status }) => {
      dispatch({
        type: FETCH_NEWS,
        payload: data,
      });
      return status === 200;
    })
    .catch(({ response }) => {
      if (response) {
        console.log(response);
      }
      return false;
    })
);


// Decide whether to use getState or pass newsId from within component
// export const increaseComments = comment => (dispatch, getState) => {
//   const newsId = getNews(getState())._id;
//   console.log(newsId);
//   console.log(comment);
//   return axios(`/news/${newsId}/increase-comments`)
//     .then(({ data }) => {
//       if (data.result) {
//         dispatch({
//           type: NEWS_INCREASE_COMMENTS,
//         });
//       }
//     })
//     .catch((error) => {
//       console.log(error);
//     });
// };

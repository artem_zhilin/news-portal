import { useState } from 'react';
import useDidUpdateEffect from './useDidUpdateEffect';

const useCarousel = (auto = 0) => {
  const [swipeElement, setSwipeElement] = useState();
  const [currentSlide, setCurrentSlide] = useState(1);
  const [changeSlideInterval, setChangeSlideInterval] = useState();

  const handleSlideChange = (direction, restartTimer = true) => {
    swipeElement[direction].call(swipeElement);
    let currentPosition = swipeElement.getPos();
    setCurrentSlide(++currentPosition);
    if (auto > 0 && restartTimer) {
      clearInterval(changeSlideInterval);
      const intervalId = setInterval(handleSlideChange.bind(swipeElement, 'next', false), auto);
      setChangeSlideInterval(intervalId);
    }
  };

  useDidUpdateEffect(() => {
    if (auto > 0) {
      const intervalId = setInterval(handleSlideChange.bind(swipeElement, 'next', false), auto);
      setChangeSlideInterval(intervalId);
    }
    return () => {
      clearInterval(changeSlideInterval);
    };
  }, [swipeElement]);

  return [setSwipeElement, handleSlideChange, currentSlide];
};

export default useCarousel;

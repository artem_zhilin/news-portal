const styles = theme => ({
  '@global': {
    html: {
      height: '100%',
      margin: 0,
    },
    body: {
      height: '100%',
      backgroundColor: theme.palette.common.white,
      padding: '0 !important',
    },
    '.text-center': {
      textAlign: 'center',
    },
    '.text-right': {
      textAlign: 'right',
    },
    '.material-icons': {
      display: 'inline-flex',
      verticalAlign: 'middle',
      '&.photo-icon': {
        color: '#40af07',
      },
      '&.video-icon': {
        color: '#d62c2c',
      },
      '&.pointer': {
        cursor: 'pointer',
      },
      '&.inner-icons': {
        marginRight: '5px',
        fontSize: '20px',
      },
      '&.icon-15': {
        fontSize: '15px',
      },
      '&.icon-20': {
        fontSize: '20px',
      },
      '&.icon-28': {
        fontSize: '28px',
      },
    },
    '.loader-spinner': {
      textAlign: 'center',
      marginTop: '20px',
    },
    '.m-r-xs': {
      marginRight: '5px',
    },
    '.m-r-sm': {
      marginRight: '10px',
    },
    '.m-r': {
      marginRight: '15px',
    },
    '.m-r-lg': {
      marginRight: '25px',
    },
    '.m-l': {
      marginLeft: '15px',
    },
    '.m-t': {
      marginTop: '15px',
    },
    '.m-t-xs': {
      marginTop: '5px',
    },
    '.m-b': {
      marginBottom: '15px',
    },
    '.m-b-md': {
      marginBottom: '20px',
    },
    '.m-b-sm': {
      marginBottom: '10px',
    },
    '.m-sides-xs': {
      margin: '0 5px',
    },
    '.p-t-md': {
      paddingTop: '20px',
    },
    '.p-r': {
      paddingRight: '15px',
    },
    '.p-r-md': {
      paddingRight: '20px',
    },
    '.p-sides': {
      padding: '0 10px',
    },
    a: {
      color: 'inherit',
      textDecoration: 'none',
      '&:hover,&:active,&:visited': {
        textDecoration: 'none',
      },
    },
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    padding: '20px 0 40px 0',
    minHeight: 'calc(100vh - 64px)',
    [theme.breakpoints.up(1200)]: {
      width: 1160,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    [theme.breakpoints.down('xs')]: {
      marginRight: theme.spacing.unit * 1.5,
      marginLeft: theme.spacing.unit * 1.5,
    },
  },
});

export default styles;

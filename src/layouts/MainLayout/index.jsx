import React, { Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import styles from './styles';
import NavBar from '../../components/NavBar';
import Footer from '../../components/Footer';
import SideBar from '../../components/SideBar';

const MainLayout = ({ classes, children }) => (
  <Fragment>
    <CssBaseline />
    <NavBar />
    <main className={classes.layout}>
      <Grid container>
        <Grid className="p-r-md" item lg={8} md={8} sm={8} xs={12}>
          {children}
        </Grid>
        <Grid item lg={4} md={4} sm={4} xs={12}>
          <SideBar />
        </Grid>
      </Grid>
    </main>
    <Footer />
  </Fragment>
);

MainLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default withStyles(styles)(MainLayout);

/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
import './i18n';
import axios from 'axios';
import App from './App';

axios.defaults.baseURL = 'http://localhost:1337';
// eslint-disable-next-line no-undef
ReactDOM.render(<App />, document.getElementById('root'));

import { configure, addDecorator } from '@storybook/react';
import React from 'react';
import MainLayout from '../stories/StoriesMainLayout';
import { useTranslation } from 'react-i18next';
import '../src/i18n';
import { Provider } from 'react-redux';
import store from '../src/store';

const TranslationProvider = ({ story }) => {
  const { t, i18n } = useTranslation();
  return (
    <MainLayout><Provider store={store}>{story({ t, i18n })}</Provider></MainLayout>
  );
}

addDecorator(storyFn => <TranslationProvider story={storyFn} />);

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /\.stories\.(js$|jsx$)/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
